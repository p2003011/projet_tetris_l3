package Model.Tetre;

import Model.ATetre;
import Model.BlockTetre;
import Model.data.Grille;
import Model.utils.BlockColor;
import Model.utils.Coord;

import java.awt.*;

public class TetreBarre extends ATetre {
    public TetreBarre(Grille grille) {
        super(grille);
    }

    @Override
    protected void build() {
        for(int i = 0; i < 4; i++) {
            listeDeBlockModel[0][i] = new BlockTetre(new Coord(i,1), BlockColor.BLUE);
        }

        for(int i = 0; i < 4; i++) {
            listeDeBlockModel[1][i] = new BlockTetre(new Coord(1,i-1), BlockColor.BLUE);
        }
        for(int i = 0; i < 4; i++) {
            listeDeBlockModel[2][i] = new BlockTetre(new Coord(i,0), BlockColor.BLUE);
        }
        for(int i = 0; i < 4; i++) {
            listeDeBlockModel[3][i] = new BlockTetre(new Coord(2,i-1), BlockColor.BLUE);
        }
    }

    @Override
    protected void fillTetrominoWallKickData() {
        tetrominoWallKickData = new Coord[4][4];

        tetrominoWallKickData[0][0] = new Coord(-2, 0);
        tetrominoWallKickData[0][1] = new Coord(1, 0);
        tetrominoWallKickData[0][2] = new Coord(-2, -1);
        tetrominoWallKickData[0][3] = new Coord(1, 2);
        tetrominoWallKickData[1][0] = new Coord(-1, 0);
        tetrominoWallKickData[1][1] = new Coord(2, 0);
        tetrominoWallKickData[1][2] = new Coord(-1, 2);
        tetrominoWallKickData[1][3] = new Coord(2, -1);
        tetrominoWallKickData[2][0] = new Coord(2, 0);
        tetrominoWallKickData[2][1] = new Coord(-1, 0);
        tetrominoWallKickData[2][2] = new Coord(2, 1);
        tetrominoWallKickData[2][3] = new Coord(-1, -2);
        tetrominoWallKickData[3][0] = new Coord(1, 0);
        tetrominoWallKickData[3][1] = new Coord(-2, 0);
        tetrominoWallKickData[3][2] = new Coord(1, -2);
        tetrominoWallKickData[3][3] = new Coord(-2, 1);
    }
}
