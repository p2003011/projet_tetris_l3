package Model.Tetre;

import Model.ATetre;
import Model.BlockTetre;
import Model.data.Grille;
import Model.utils.BlockColor;
import Model.utils.Coord;

import java.awt.*;

public class TetreT extends ATetre {
    public TetreT(Grille grille) {
        super(grille);
    }

    @Override
    protected void build() {
        for(int i = 0; i < 3; i++) {
            listeDeBlockModel[0][i] = new BlockTetre(new Coord(i,1), BlockColor.PINK);
        }
        listeDeBlockModel[0][3] = new BlockTetre(new Coord(1,0), BlockColor.PINK);


        for(int i = 0; i < 3; i++) {
            listeDeBlockModel[1][i] = new BlockTetre(new Coord(1, i ), BlockColor.PINK);
        }
        listeDeBlockModel[1][3] = new BlockTetre(new Coord(2,1), BlockColor.PINK);

        for(int i = 0; i < 3; i++) {
            listeDeBlockModel[2][i] = new BlockTetre(new Coord(i, 1), BlockColor.PINK);
        }
        listeDeBlockModel[2][3] = new BlockTetre(new Coord(1,2), BlockColor.PINK);

        for(int i = 0; i < 3; i++) {
            listeDeBlockModel[3][i] = new BlockTetre(new Coord(1, i), BlockColor.PINK);
        }
        listeDeBlockModel[3][3] = new BlockTetre(new Coord(0,1), BlockColor.PINK);
    }
}
