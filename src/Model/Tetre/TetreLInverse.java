package Model.Tetre;

import Model.ATetre;
import Model.BlockTetre;
import Model.data.Grille;
import Model.utils.BlockColor;
import Model.utils.Coord;

import java.awt.*;

public class TetreLInverse extends ATetre {
    public TetreLInverse(Grille grille) {
        super(grille);
    }

    @Override
    protected void build() {
        Color purple = new Color(120, 37, 111);


        for(int i = 0; i < 3; i++) {
            listeDeBlockModel[0][i] = new BlockTetre(new Coord(i,1), BlockColor.PURPLE);
        }
        listeDeBlockModel[0][3] = new BlockTetre(new Coord(0,0), BlockColor.PURPLE);


        for(int i = 0; i < 3; i++) {
            listeDeBlockModel[1][i] = new BlockTetre(new Coord(1, i), BlockColor.PURPLE);
        }
        listeDeBlockModel[1][3] = new BlockTetre(new Coord(2,0), BlockColor.PURPLE);

        for(int i = 0; i < 3; i++) {
            listeDeBlockModel[2][i] = new BlockTetre(new Coord(i, 1), BlockColor.PURPLE);
        }
        listeDeBlockModel[2][3] = new BlockTetre(new Coord(2,2), BlockColor.PURPLE);

        for(int i = 0; i < 3; i++) {
            listeDeBlockModel[3][i] = new BlockTetre(new Coord(1, i), BlockColor.PURPLE);
        }
        listeDeBlockModel[3][3] = new BlockTetre(new Coord(0,2), BlockColor.PURPLE);
    }
}
