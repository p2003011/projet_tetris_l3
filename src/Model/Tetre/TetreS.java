package Model.Tetre;

import Model.ATetre;
import Model.BlockTetre;
import Model.data.Grille;
import Model.utils.BlockColor;
import Model.utils.Coord;

import java.awt.*;

public class TetreS extends ATetre {
    public TetreS(Grille grille) {
        super(grille);
    }

    @Override
    protected void build() {
        listeDeBlockModel[0][0] = new BlockTetre(new Coord(1,0), BlockColor.GREEN);
        listeDeBlockModel[0][1] = new BlockTetre(new Coord(2,0), BlockColor.GREEN);
        listeDeBlockModel[0][2] = new BlockTetre(new Coord(0,1), BlockColor.GREEN);
        listeDeBlockModel[0][3] = new BlockTetre(new Coord(1,1), BlockColor.GREEN);

        listeDeBlockModel[1][0] = new BlockTetre(new Coord(1,0), BlockColor.GREEN);
        listeDeBlockModel[1][1] = new BlockTetre(new Coord(1,1), BlockColor.GREEN);
        listeDeBlockModel[1][2] = new BlockTetre(new Coord(2,1), BlockColor.GREEN);
        listeDeBlockModel[1][3] = new BlockTetre(new Coord(2,2), BlockColor.GREEN);

        listeDeBlockModel[2][0] = new BlockTetre(new Coord(1,0), BlockColor.GREEN);
        listeDeBlockModel[2][1] = new BlockTetre(new Coord(2,0), BlockColor.GREEN);
        listeDeBlockModel[2][2] = new BlockTetre(new Coord(0,1), BlockColor.GREEN);
        listeDeBlockModel[2][3] = new BlockTetre(new Coord(1,1), BlockColor.GREEN);

        listeDeBlockModel[3][0] = new BlockTetre(new Coord(0,0), BlockColor.GREEN);
        listeDeBlockModel[3][1] = new BlockTetre(new Coord(0,1), BlockColor.GREEN);
        listeDeBlockModel[3][2] = new BlockTetre(new Coord(1,1), BlockColor.GREEN);
        listeDeBlockModel[3][3] = new BlockTetre(new Coord(1,2), BlockColor.GREEN);
    }
}
