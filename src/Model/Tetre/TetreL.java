package Model.Tetre;

import Model.ATetre;
import Model.BlockTetre;
import Model.data.Grille;
import Model.utils.BlockColor;
import Model.utils.Coord;

import java.awt.*;

public class TetreL extends ATetre {
    public TetreL(Grille grille) {
        super(grille);
    }

    @Override
    protected void build() {
        for(int i = 0; i < 3; i++) {
            listeDeBlockModel[0][i] = new BlockTetre(new Coord(i,1), BlockColor.ORANGE);
        }
        listeDeBlockModel[0][3] = new BlockTetre(new Coord(2,0), BlockColor.ORANGE);


        for(int i = 0; i < 3; i++) {
            listeDeBlockModel[1][i] = new BlockTetre(new Coord(1, i), BlockColor.ORANGE);
        }
        listeDeBlockModel[1][3] = new BlockTetre(new Coord(2,2), BlockColor.ORANGE);

        for(int i = 0; i < 3; i++) {
            listeDeBlockModel[2][i] = new BlockTetre(new Coord(i, 1), BlockColor.ORANGE);
        }
        listeDeBlockModel[2][3] = new BlockTetre(new Coord(0,2), BlockColor.ORANGE);

        for(int i = 0; i < 3; i++) {
            listeDeBlockModel[3][i] = new BlockTetre(new Coord(1, i), BlockColor.ORANGE);
        }
        listeDeBlockModel[3][3] = new BlockTetre(new Coord(0,0), BlockColor.ORANGE);
    }
}
