package Model.Tetre;

import Model.ATetre;
import Model.BlockTetre;
import Model.data.Grille;
import Model.utils.BlockColor;
import Model.utils.Coord;

import java.awt.*;

public class TetreCarre extends ATetre {
    public TetreCarre(Grille grille) {
        super(grille);
    }

    @Override
    protected void build() {
        for(int i = 0; i < 2; i++) {
            for(int j = 0; j < 2; j++) {
                listeDeBlockModel[0][i + j * 2] = new BlockTetre(new Coord(i,j), BlockColor.YELLOW);
            }
        }
    }

    @Override
    public boolean rotate() {
        // No rotation

        return true;
    }
}
