package Model.Tetre;

import Model.ATetre;
import Model.BlockTetre;
import Model.data.Grille;
import Model.utils.BlockColor;
import Model.utils.Coord;

import java.awt.*;

public class TetreSInverse extends ATetre {
    public TetreSInverse(Grille grille) {
        super(grille);
    }

    @Override
    protected void build() {
        for(int i = 0; i < 2; i++) {
            for(int j = 0; j < 2; j++) {
                listeDeBlockModel[0][i + j * 2] = new BlockTetre(new Coord(i +j,j), BlockColor.RED);
            }
        }

        listeDeBlockModel[1][0] = new BlockTetre(new Coord(2,0), BlockColor.RED);
        listeDeBlockModel[1][1] = new BlockTetre(new Coord(2,1), BlockColor.RED);
        listeDeBlockModel[1][2] = new BlockTetre(new Coord(1,1), BlockColor.RED);
        listeDeBlockModel[1][3] = new BlockTetre(new Coord(1,2), BlockColor.RED);

        for(int i = 0; i < 2; i++) {
            for(int j = 0; j < 2; j++) {
                listeDeBlockModel[2][i + j * 2] = new BlockTetre(new Coord(i +j,j), BlockColor.RED);
            }
        }

        listeDeBlockModel[3][0] = new BlockTetre(new Coord(1,0), BlockColor.RED);
        listeDeBlockModel[3][1] = new BlockTetre(new Coord(1,1), BlockColor.RED);
        listeDeBlockModel[3][2] = new BlockTetre(new Coord(0,1), BlockColor.RED);
        listeDeBlockModel[3][3] = new BlockTetre(new Coord(0,2), BlockColor.RED);
    }
}
