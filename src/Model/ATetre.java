package Model;

import static java.lang.Math.abs;
import Model.data.Grille;
import Model.data.Score;
import Model.utils.Coord;
import Model.utils.Movement;

/**
 * Crée par Théo
 * Cette classe est la classe mère abstraite de tous les Tetromino.
 * Un Tetromino est constitué de 4 Block qui suit un pattern spécifique et une rotation spécifique.
 * Un tetromino auras un model, qui vas être dérivés en 4 états: 1 pour chaque rotation
 * et auras une version "réél" qui sera celle sur la grille.
 * Cette version réél vas être mise à jour par les facteurs de l'origine (la position du Tetromino sur la grille)
 * et par le model actuel (la rotation du Tetromino)
 */
public abstract class ATetre {
// Fields
    private Grille grille;

    protected BlockTetre[] listeDeBlock;
    protected BlockTetre[][] listeDeBlockModel;
    private int currentModel = 0;
    private Coord origin;
    private boolean hasBeenSwap;

    protected Coord[][] tetrominoWallKickData; // state, test


// Properties
    public BlockTetre[] getCurrentListDeBlocModel() {
    return listeDeBlockModel[currentModel];
}
    public BlockTetre[] getListDeBlocModel() {
        return listeDeBlockModel[0];
    }
    public BlockTetre[] getListeDeBlock() { return  listeDeBlock; }

    public int getLargeurTetre() {
        int minX = listeDeBlockModel[0][0].getCoord().x;
        int maxX = listeDeBlockModel[0][0].getCoord().x;
        for(BlockTetre block : listeDeBlockModel[0]) {
            if(maxX < block.getCoord().x)
                maxX = block.getCoord().x;
            if(minX > block.getCoord().x) {
                minX = block.getCoord().x;
            }
        }

        return abs(maxX - minX) + 1;
    }
    public boolean checkIfPositionValid() {
        return checkIfPositionValid(listeDeBlock);
    }

    private BlockTetre[] getUpdatedPosition() {
        BlockTetre[] newListeDeBLock = new BlockTetre[4];

        for(int i = 0; i < 4; i++) {
            Coord newCoord = Coord.plus(origin, listeDeBlockModel[currentModel][i].getCoord());
            newListeDeBLock[i] = new BlockTetre(newCoord, listeDeBlockModel[currentModel][i].getBlockColor());
        }

        return newListeDeBLock;
    }

// Constructor
    public ATetre(Grille grille) {
        this.grille = grille;
        listeDeBlockModel = new BlockTetre[4][4];
        listeDeBlock = new BlockTetre[4];
        build();
        fillTetrominoWallKickData();
    }

// Checker
    private boolean checkIfPositionValid(BlockTetre[] listeDeBlockToTest) {
        for(BlockTetre toTest : listeDeBlockToTest) {
            if(toTest.getCoord().x < 0 || toTest.getCoord().x >= grille.getLargeur()
            || toTest.getCoord().y >= grille.getHauteur())
                return false;

            if(toTest.getCoord().y < 0 )
                return true;
            if(grille.getCase(toTest.getCoord().x, toTest.getCoord().y) != null)
                return false;
        }

        return true;
    }

    public boolean isThereSomethingUnderMe() {
        for(BlockTetre block : listeDeBlock) {
            if(grille.getHauteur() <= block.getCoord().y + 1) {
                return true;
            }
            if(grille.getCase(block.getCoord().x, block.getCoord().y + 1) != null) {
                return true;
            }
        }

        return false;
    }


// Virtual Methods
    abstract protected void build();

    protected void fillTetrominoWallKickData() {
        tetrominoWallKickData = new Coord[4][4];

        tetrominoWallKickData[0][0] = new Coord(-1, 0);
        tetrominoWallKickData[0][1] = new Coord(-1, 1);
        tetrominoWallKickData[0][2] = new Coord(0, -2);
        tetrominoWallKickData[0][3] = new Coord(-1, -2);
        tetrominoWallKickData[1][0] = new Coord(1, 2);
        tetrominoWallKickData[1][1] = new Coord(1, -1);
        tetrominoWallKickData[1][2] = new Coord(0, 2);
        tetrominoWallKickData[1][3] = new Coord(1, 2);
        tetrominoWallKickData[2][0] = new Coord(1, 0);
        tetrominoWallKickData[2][1] = new Coord(1, 1);
        tetrominoWallKickData[2][2] = new Coord(0, -2);
        tetrominoWallKickData[2][3] = new Coord(1, -2);
        tetrominoWallKickData[3][0] = new Coord(-1, 0);
        tetrominoWallKickData[3][1] = new Coord(-1, 1);
        tetrominoWallKickData[3][2] = new Coord(0, 2);
        tetrominoWallKickData[3][3] = new Coord(-1, 2);
    }



// Public main Methods
    public void spawn() {
        currentModel = 0;
        origin = new Coord(grille.getLargeur()/2 - getLargeurTetre()/2 - (getLargeurTetre()%2 == 0? 0 : 1), 0);
        listeDeBlock = getUpdatedPosition();

        if(!checkIfPositionValid(listeDeBlock)) {
            origin = Coord.moins(origin, new Coord(0, 1));
            listeDeBlock = getUpdatedPosition();
        }
    }



    public boolean rotate() {
        currentModel = (currentModel + 1) % 4;

        Coord backupOrigin = origin.clone();

        BlockTetre[] newListeDeBLock = getUpdatedPosition();

        int i = 0;

        while(!checkIfPositionValid(newListeDeBLock) && i <= 3) {
            origin = backupOrigin.clone();

            origin = Coord.plus(origin, tetrominoWallKickData[currentModel][i]);

            newListeDeBLock = getUpdatedPosition();

            i++;
        }

        if(i <= 3) {
            listeDeBlock = newListeDeBLock;
            return true;
        }
        currentModel = (currentModel - 1) % 4;
        origin = backupOrigin;
        return false;
    }


    public boolean move(Movement movement, Score score) {
        Coord directionCoord = new Coord(0,0);

        switch(movement) {
            case ROTATE:
                return rotate();
            case GAUCHE:
                directionCoord = new Coord(-1, 0);
                break;
            case DROITE:
                directionCoord = new Coord(1, 0);
                break;
            case BAS:
                directionCoord = new Coord(0, 1);
                break;
            case DROP:
                drop(score);
                break;
        }

        origin = Coord.plus(origin, directionCoord);
        BlockTetre[] newListeDeBLock = getUpdatedPosition();

        if(!checkIfPositionValid(newListeDeBLock)) {
            origin = Coord.moins(origin, directionCoord);
            return false;
        }

        listeDeBlock = newListeDeBLock;
        return true;
    }

    public BlockTetre[] getLowestBlockPos(){
        Coord directionCoord = new Coord(0, 1);
        Coord OrinalOrigin = new Coord(origin.x, origin.y);
        BlockTetre[] newListeDeBLock;
        int i = 0;

        do {
            origin = Coord.plus(origin, directionCoord);
            i++;
        }while (checkIfPositionValid(getUpdatedPosition()));
        origin = Coord.moins(origin, directionCoord);
        if(i > 1) {

            newListeDeBLock = getUpdatedPosition();
            origin = OrinalOrigin;
            for (BlockTetre block : newListeDeBLock) {
                block.switchToTransparent();
            }
            return newListeDeBLock;
        }
        origin = OrinalOrigin;
        return null;

    }

    public void drop(Score score){
        while (move(Movement.BAS, score)){
            score.addToScore(10* score.getNiveau());
        }
    }



}
