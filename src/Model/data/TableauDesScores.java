package Model.data;

import java.io.*;
import java.util.*;

/**
 * Crée par Jérémie
 * Le tableau des Scores est affichés sur le menu principal, il parse les informations d'un fichier textes pour
 * recréer des Score qui peuvent ensuite être trier.
 */
public class TableauDesScores {
    private ArrayList<Score> tableauDesScores;

    public TableauDesScores() {
        this.tableauDesScores = new ArrayList();
        loadFromFile();
    }

    public ArrayList<Score> getTableauDesScores() {
        return tableauDesScores;
    }

    public void setTableauDesScores(ArrayList<Score> tableauDesScores) {
        this.tableauDesScores = tableauDesScores;
    }

    public void addScore(Score score) {
        this.tableauDesScores.add(score);
        this.sort();
        saveToFile();
    }

    public void sort() {
        Collections.sort(this.tableauDesScores);
    }

    public void loadFromFile() {
        try {
            FileReader fileReader = new FileReader("src/Data/scores.txt");
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                String[] parts = line.split(", ");
                if (parts.length == 2) {
                    String stringValue = parts[0];
                    int intValue = Integer.parseInt(parts[1]);
                    Score score = new Score(intValue, stringValue);
                    this.tableauDesScores.add(score);

                } else {
                    System.err.println("Format invalide pour la ligne : " + line);
                }
            }

            bufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void saveToFile() {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter("src/Data/scores.txt"))) {
            for (Score score : tableauDesScores) {
                // Écrivez chaque objet dans une nouvelle ligne du fichier
                writer.write(score.getPlayerName()+ ", " + score.getScore());
                writer.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
