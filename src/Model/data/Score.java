package Model.data;

/**
 * Crée par Jérémie
 * Cette classe enregistre le score actuel, mais aussi le nombre de ligne brisée et le niveau (difficulté) de la partie
 */
public class Score implements Comparable<Score> {
    int score;

    int niveau;

    int nbLigne;
    String playerName;

    public Score(int score, String playerName) {
        this.score = score;
        this.playerName = playerName;
    }

    public Score() {
        this.score = 0;
        this.playerName = "None";
        this.nbLigne = 0;
        this.niveau = 1;
    }

    public int getScore() {
        return score;
    }

    public int getNiveau() {
        return niveau;
    }

    public int getNbLigne() {
        return nbLigne;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void addToScore(int value) { score += value; }
    public void addLigne(int value) { nbLigne += value; }
    public void incrementNiveau() { niveau++; }

    @Override
    public int compareTo(Score o) {
        return o.getScore() - this.getScore();
    }
}
