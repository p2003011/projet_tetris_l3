package Model.data;


import Model.ATetre;
import Model.BlockTetre;

/**
 * Crée par Théo
 * Cette classe gère les informations de la grille, tel que sa taille et les BlockTetre qui sont
 * à l'intérieur ou non.
 * Le Tetromino actuel (qui tombe) n'est pas enregistré ici, cependant il y a des fonctions permettant de
 * valider sa position en transformant le tetromino en simple blocs enregistré dans la grille,
 * et des fonctions pour valider une ligne complete, et la supprimé.
*/
public class Grille {
    private BlockTetre[][] grille;
    private int largeur;
    private int hauteur;

    public Grille(int largeur, int hauteur) {
        this.largeur = largeur;
        this.hauteur = hauteur;

        grille = new BlockTetre[hauteur][largeur];
    }

    public BlockTetre[][] getGrille() {
        return grille;
    }

    public BlockTetre getCase(int x , int y) {
        return grille[y][x];
    }

    public int getLargeur() {
        return largeur;
    }

    public int getHauteur() {
        return hauteur;
    }

    public void reset() {
        grille = new BlockTetre[hauteur][largeur];
    }
    public boolean fillGridWithTetre(ATetre tetre){
        if (tetre == null)
            return false;
        for(BlockTetre block : tetre.getListeDeBlock()) {
            if(block.getCoord().y < 0)
                return false;
            grille[block.getCoord().y][block.getCoord().x] = block;
        }


        return true;
    }

    private int checkifLineComplete() { // return line index or -1 if no line
        for(int y = 0; y < hauteur; y++) {
            boolean lineComplete = true;
            for(int x = 0; x < largeur; x++) {
                if(grille[y][x] == null) {
                    lineComplete = false;
                    break;
                }
            }
            if(lineComplete) {
                return y;
            }
        }

        return -1;
    }

    private void deleteLine(int lineIndex) {
        for(int y = lineIndex; y > 0; y--) {
            grille[y] = grille[y-1].clone();
        }
    }

    public int completeLine() {
        int lineCounter = 0;
        while(checkifLineComplete() != -1) {
            deleteLine(checkifLineComplete());
            lineCounter++;
        }

        return lineCounter;
    }

}
