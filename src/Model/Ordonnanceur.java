package Model;

/**
 * Créée par Théo
 * L'Ordonnanceur est le thread qui gère la partie, c'est lui qui va rythmer la chute des pièces et la validation d'un Tetromino au sol.
 * Ces actions sont gérées par la partie, ce Thread a pour principale occupation de gérer les timers.
 *
 * Pour cela, il y a un système de pause en place. En plus, la durée entre chaque événement peut être rallongée grâce à la fonction addSleepTime
 * qui sera utilisé au moment où, alors qu'un tetromino s'apprête à toucher le sol, le joueur fait tourner la pièce.
 * Dans le jeu officiel, cela a pour effet de rallonger la durée avant le prochain événement, ce qui est ici assuré.
 *  En plus, la difficulté de la partie est prise en compte par l'ordonnanceur.
 */
public class Ordonnanceur  extends Thread {
// Fields
    private int addedSleepTime = 0;
    private int sleepDuration = 1000;
    private boolean stop = false;
    private boolean pause = false;
    private long timedForceRun = -1;
    private int difficulty = 1;

    public Runnable monRunnable;

// Properties
    public void addSleepTime() {
        addedSleepTime += (int) Math.log(sleepDuration / (addedSleepTime+1.0));
    }
    public void setStop(boolean bool) { stop = bool; }
    public void setPause(boolean bool) { pause = bool; }
    public void setDifficulty(int difficulty) { this.difficulty = difficulty; }
    public boolean getStop() { return stop; }
    public boolean getPause() { return pause; }

// Constructor
    public Ordonnanceur(Runnable monRunnable, int difficulty) {
        this.monRunnable = monRunnable;
        this.difficulty = difficulty;

        init();
    }

// Public Methods

    public void init() {
        setStop(false);
        setPause(false);

        difficulty = 1;
        timedForceRun = -1;
        sleepDuration = 1000;
    }

    public void forceRun() {
        timedForceRun = System.currentTimeMillis();

        monRunnable.run();
    }

    @Override
    public void run() {
        while(!stop) {
            while(pause) {
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            try {
                sleepDuration = 1000 / difficulty;

                Thread.sleep(sleepDuration + addedSleepTime);

                if(timedForceRun != -1) {
                    if(stop) {
                        break;
                    }
                    long deltaTime = System.currentTimeMillis() - timedForceRun;
                    Thread.sleep( Math.max(sleepDuration + addedSleepTime - deltaTime, 0));

                    timedForceRun = -1;
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            monRunnable.run();
        }

        System.out.println("J'ai fini");
    }
}