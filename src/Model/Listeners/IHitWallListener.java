package Model.Listeners;

import Model.utils.Movement;

public interface IHitWallListener {
    void hitWall(Movement movement);
}
