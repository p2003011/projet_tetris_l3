package Model;

import Model.Listeners.*;
import Model.Tetre.*;
import Model.data.Grille;
import Model.data.Score;
import Model.utils.Movement;
import Model.utils.Sound;
import Vue.AudioManager;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Random;

import static Model.utils.Movement.*;

/**
 * Crée par Théo
 * * Partie implémente Runnable permettant à l'ordonnanceur de rythmés les appels de la fonction run, qui va servir de boucle de jeu principal.
 * *
 * * Cette classe va gérer le tetromino actuelle (qui tombe) celui qui est hold (tombait, mais a été mis en attente, prêt à être remplacé dès le prochain tetre)
 * * et la liste des prochains tetromino générés aléatoirement, suivant le système officiel du jeu Tetris : Un sac des 7 tetromino possibles est créé, puis mélangé
 * * on pioche chaque tetromino du sac pour faire la liste, une fois le sac vide, on le remplit à nouveau de la même manière. Cela signifie qu'il peut y avoir au maximum
 * * 2 fois la même pièce à la suite, et un grand maximum de 12 pièces entre 2 mêmes tetromino
 * *
 * * C'est aussi dans cette classe que l'on va vérifier si un tetromino est prêt à être validé et posé au sol, si une ligne ou plus est complète et ici que l'on va gérer.
 * * les mouvements du tetromino actuel. (droite, gauche, bas, drop et rotations)
 * *
 * * Finalement, c'est cette classe qui va calculer le score et faire les appels aux différents listeners qui vont permettre de mettre à jour l'affichage.
 * * (la majorité de ces listeners ont été placés par Jérémie, Théo en a mis en place quelques-uns en suivant la méthode de Jérémie.)
 */
public class Partie implements Runnable {
// Fields
    Ordonnanceur ordonnanceur;

    AudioManager audioManager;

    Grille grille;
    Score score;

    ATetre holdedTetre = null;
    ATetre currentTetre = null;
    LinkedList<ATetre> nextTetreList = new LinkedList<>();
    LinkedList<ATetre> randomBagOfTetre = new LinkedList<>();
    boolean hasExhangedTetreThisRound = false;

    ArrayList<IGrilleUpdateListener> grilleUpdateListeners = new ArrayList<>();
    ArrayList<IScoreUpdateListener> scoreUpdateListeners = new ArrayList<>();
    ArrayList<IPieceFuturUpdateListener> nextTetreUpdateListeners = new ArrayList<>();
    ArrayList<IPieceRetenueUpdateListener> holdedTetreUpdateListeners = new ArrayList<>();
    ArrayList<IlineCompletListener> pieceSpawnListeners = new ArrayList<>();
    ArrayList<IHitWallListener> hitWallListeners = new ArrayList<>();
    ArrayList<IPartieFiniListener> partieFiniListeners = new ArrayList<>();
    ArrayList<ISetPauseListener> setPauseListeners = new ArrayList<>();
    ArrayList<ISetJeuListener> setJeuListeners = new ArrayList<>();


// Properties
    public ATetre getHoldedTetre() { return holdedTetre; }
    public ATetre getCurrentTetre() { return currentTetre; }
    public LinkedList<ATetre> getNextTetreList() { return nextTetreList; }
    public boolean getHasExhangedTetreThisRound() { return hasExhangedTetreThisRound; }

    public Score getScore() {
        return score;
    }

    public Grille getGrille() {
        return grille;
    }

    public boolean getIsPaused() {
        return ordonnanceur.getPause();
    }

    public void togglePause() {
        if(ordonnanceur.getPause()) {
            ordonnanceur.setPause(false);

            for (ISetJeuListener listener : setJeuListeners) {
                listener.partieJeu();
            }
        }
        else {
            ordonnanceur.setPause(true);

            for (ISetPauseListener listener : setPauseListeners) {
                listener.partiePause();
            }
        }
    }

    public void exchangeHoldedTetre() {
        if(currentTetre != null && !hasExhangedTetreThisRound) {
            audioManager.playSound(Sound.SWAP, false);
            currentTetre.spawn();
            ATetre tmp = currentTetre;

            currentTetre = holdedTetre;
            holdedTetre = tmp;

            if(currentTetre == null) {
                setUpNextTetre();
            }
            hasExhangedTetreThisRound = true;

            for(IPieceRetenueUpdateListener listener : holdedTetreUpdateListeners) {
                listener.pieceRetenueUpdate();
            }
            for (IPieceFuturUpdateListener listener : nextTetreUpdateListeners) {
                listener.pieceFuturUpdate();
            }
            for (IGrilleUpdateListener listener : grilleUpdateListeners) {
                listener.grilleUpdate();
            }
        }
    }

    public void addGrilleUpdateListener(IGrilleUpdateListener listener) {
        grilleUpdateListeners.add(listener);
    }

    public void addScoreUpdateListener(IScoreUpdateListener listener) {
        scoreUpdateListeners.add(listener);
    }

    public void addNextTetreUpdateListener(IPieceFuturUpdateListener listener) {
        nextTetreUpdateListeners.add(listener);
    }

    public void addHoldedTetreUpdateListener(IPieceRetenueUpdateListener listener) {
        holdedTetreUpdateListeners.add(listener);
    }

    public void addLineCompletListener(IlineCompletListener listener) {
        pieceSpawnListeners.add(listener);
    }
    public void addHitWallListener(IHitWallListener listener) {
        hitWallListeners.add(listener);
    }

    public void addPartieFiniListener(IPartieFiniListener listener) {
        partieFiniListeners.add(listener);
    }
    public void addSetJeuListener(ISetJeuListener listener) {
        setJeuListeners.add(listener);
    }
    public void addSetPauseListener(ISetPauseListener listener) {
        setPauseListeners.add(listener);
    }

// Constructor
    public Partie(Grille grille) {
        this.grille = grille;
        score = new Score();
    }

// Private Methods
    private ATetre getRandomTetre() {
        if(randomBagOfTetre.isEmpty()) {
            fillRandomBagOfTetre();
        }

        return randomBagOfTetre.removeFirst();
    }

    private void fillRandomBagOfTetre() {
        randomBagOfTetre.clear();

        Random random = new Random();

        ArrayList<ATetre> toMix = new ArrayList<>();
        toMix.add( new TetreBarre(grille));
        toMix.add( new TetreCarre(grille));
        toMix.add( new TetreL(grille));
        toMix.add( new TetreLInverse(grille));
        toMix.add( new TetreS(grille));
        toMix.add( new TetreSInverse(grille));
        toMix.add( new TetreT(grille));

        while(!toMix.isEmpty()) {
            randomBagOfTetre.addLast(toMix.remove(random.nextInt(0, toMix.size())));
        }
    }

    private int calculScoreLigneComplete(int lineCounter) {
        return lineCounter * 1000 * score.getNiveau();
    }

    private boolean setUpNextTetre() {
        currentTetre = nextTetreList.removeFirst();
        currentTetre.spawn();

        if(!currentTetre.checkIfPositionValid()) {
            currentTetre = null;
            return false;
        }

        nextTetreList.addLast(getRandomTetre());


        for (IPieceFuturUpdateListener listener : nextTetreUpdateListeners) {
            listener.pieceFuturUpdate();
        }

        return true;
    }

    private void endGame() {
        if(!ordonnanceur.getStop()) {
            ordonnanceur.setStop(true);
            audioManager.stopLoopClip();
            audioManager.playSound(Sound.GAMEOVER, false);
            System.out.println("END GAME");

            for (IPartieFiniListener listener : partieFiniListeners) {
                listener.partieFini();
            }
        }
        else {
            System.out.println("je suis bugué");
            ordonnanceur.setStop(true);
        }
    }

// Public Methods
    public void init() {
        score = new Score();

        this.audioManager = AudioManager.getInstance();

        fillRandomBagOfTetre();

        for(int i = 0; i < 5; i++) {
            nextTetreList.add(getRandomTetre());
        }

        if(ordonnanceur != null)
            ordonnanceur.setStop(true);
        ordonnanceur = new Ordonnanceur(this, score.getNiveau());
        ordonnanceur.start();

        audioManager.stopLoopClip();

        audioManager.playSound(Sound.MUSIC, true);
    }

    public void reset() {

        fillRandomBagOfTetre();
        holdedTetre = null;
        currentTetre = null;
        nextTetreList = new LinkedList<>();
        randomBagOfTetre = new LinkedList<>();
        hasExhangedTetreThisRound = false;

        grille.reset();

        init();

        for (IGrilleUpdateListener listener : grilleUpdateListeners) {
            listener.grilleUpdate();
        }
        for (IPieceFuturUpdateListener listener : nextTetreUpdateListeners) {
            listener.pieceFuturUpdate();
        }
        for (IPieceRetenueUpdateListener listener : holdedTetreUpdateListeners) {
            listener.pieceRetenueUpdate();
        }
    }


    @Override
    public void run() {
        if(currentTetre != null) {
            if (currentTetre.isThereSomethingUnderMe()) {
                // le tetre à touché le sol, on le valide
                if(!grille.fillGridWithTetre(currentTetre)) { // Did we lose? if not the Tetre is placed and we go on
                    endGame();
                } else {
                    // currenTetre passe à null
                    currentTetre = null;
                    // Check if line complete
                    // et ajoute des points au score en fonction
                    int lineCounter = grille.completeLine();
                    score.addToScore(calculScoreLigneComplete(lineCounter));
                    score.addLigne(lineCounter);
                    if(score.getNbLigne() - score.getNiveau()* 2 >= 0) { // toute les 2 lignes on monte de niveau (5 est plus équilibrés, mais 2 c'est bien pour la présentation)
                        score.incrementNiveau();
                        ordonnanceur.setDifficulty(score.getNiveau());
                    }

                    if (lineCounter > 0) {
                        audioManager.playSound(Sound.LINEBREAK, false);
                        for (IlineCompletListener listener : pieceSpawnListeners) {
                            listener.LineComplet(lineCounter);
                        }
                    }
                    hasExhangedTetreThisRound = false;
                }
            }
            for (IScoreUpdateListener listener : scoreUpdateListeners) {
                listener.scoreUpdate();
            }
        }


        if(currentTetre == null) {
            if(!setUpNextTetre()) {
                endGame();
            }
        }
        else {
            currentTetre.move(Movement.BAS, score);
        }

        for(IGrilleUpdateListener listener : grilleUpdateListeners) {
            listener.grilleUpdate();
        }
        for (IPieceFuturUpdateListener listener : nextTetreUpdateListeners) {
            listener.pieceFuturUpdate();
        }
    }
// Contrôleur function
    public void move(Movement movement) {
        if(currentTetre != null && !ordonnanceur.getPause() && !ordonnanceur.getStop()) {


            if(!this.currentTetre.move(movement, score) && (movement == GAUCHE || movement == DROITE || movement == DROP)) {
                for (IHitWallListener listener : hitWallListeners) {
                    listener.hitWall(movement);
                }
            }

            if(movement != DROP && movement != ROTATE) {
                audioManager.playSound(Sound.MOVE, false);
            }

            switch(movement) {
                case ROTATE :
                    audioManager.playSound(Sound.ROTATE, false);

                    if(currentTetre.isThereSomethingUnderMe()) {
                        ordonnanceur.addSleepTime();
                    }
                    break;
                case DROP:
                    audioManager.playSound(Sound.DROP, false);
                    ordonnanceur.forceRun();
                    break;
                case BAS:
                    score.addToScore(10* score.getNiveau());
            }

            for (IGrilleUpdateListener listener : grilleUpdateListeners) {
                listener.grilleUpdate();
            }
            for (IPieceFuturUpdateListener listener : nextTetreUpdateListeners) {
                listener.pieceFuturUpdate();
            }
            for (IPieceRetenueUpdateListener listener : holdedTetreUpdateListeners) {
                listener.pieceRetenueUpdate();
            }
            for (IScoreUpdateListener listener : scoreUpdateListeners) {
                listener.scoreUpdate();
            }
        }
    }

}
