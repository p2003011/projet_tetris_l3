package Model.utils;

public enum Sound {
    MUSIC,
    MOVE,
    ROTATE,
    LINEBREAK,
    GAMEOVER,
    DROP,
    SWAP
}
