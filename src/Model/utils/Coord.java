package Model.utils;

/**
 * Crée par Théo
 * Coord est notre classe pour gérer les position des Tetre et des blocks dans l'espace de la grille.
 * sur un plus gros projet elle pourrait implémenter des fonctions d'un vecteur 2D, comme le caclul de distance ou la translation
 */
public class Coord implements Cloneable {
    public int x;
    public int y;

    public Coord(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public static Coord plus(Coord A, Coord B) {
        return new Coord(A.x + B.x, A.y + B.y);
    }
    public static Coord moins(Coord A, Coord B) {
        return new Coord(A.x - B.x, A.y - B.y);
    }

    @Override
    public Coord clone() {
        try {
            return (Coord) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new AssertionError();
        }
    }
}