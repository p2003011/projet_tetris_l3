package Model.utils;

/**
 * Crée par Théo
 * Cette classe permet de simplifier la compréhension des contrôles clavier et donc le code des contrôleurs.
 * Elle représente les mouvements qu'un tetromino peut effectuer et qui peuvent être demandés par le joueur via le clavier.
 */
public enum Movement {
    ROTATE,
    BAS,
    GAUCHE,
    DROITE,
    DROP
}
