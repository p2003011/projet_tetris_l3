package Model.utils;

public enum BlockColor {
    BLUE,
    ORANGE,
    YELLOW,
    GREEN,
    PINK,
    PURPLE,
    RED,
    T_BLUE,
    T_ORANGE,
    T_YELLOW,
    T_GREEN,
    T_PINK,
    T_PURPLE,
    T_RED,
    EMPTY

}
