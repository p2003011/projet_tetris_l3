package Model;

import Model.utils.BlockColor;
import Model.utils.Coord;

import java.awt.*;
import java.util.HashMap;

/**
 * Crée par Théo
 * Un BlocTetre est la base de construction d'un Tetromino, il s'agit également des éléments constituant la grille.
 * Il contient une Coordonnée, qui permettra de la placer sur la grille et de valider sa position
 * et une couleur, définie par son Tetromino original.
 */
public class BlockTetre {
        private Coord coord;


        private BlockColor blockColor;

        public BlockTetre(Coord coord, BlockColor color) {
            this.coord = coord;
            this.blockColor = color;
        }

        public BlockColor getBlockColor() {
            return blockColor;
        }

        public Coord getCoord() {
            return coord;
        }
        public void setCoord(Coord coord) {
            this.coord = coord;
        }


    public Image getSprit(HashMap<BlockColor, Image> spritesColors) {
            switch (blockColor) {
                case BLUE:
                    return spritesColors.get(BlockColor.BLUE);
                case ORANGE:
                    return spritesColors.get(BlockColor.ORANGE);
                case YELLOW:
                    return spritesColors.get(BlockColor.YELLOW);
                case GREEN:
                    return spritesColors.get(BlockColor.GREEN);
                case PINK:
                    return spritesColors.get(BlockColor.PINK);
                case PURPLE:
                    return spritesColors.get(BlockColor.PURPLE);
                case RED:
                    return spritesColors.get(BlockColor.RED);
                case T_BLUE:
                    return spritesColors.get(BlockColor.T_BLUE);
                case T_ORANGE:
                    return spritesColors.get(BlockColor.T_ORANGE);
                case T_YELLOW:
                    return spritesColors.get(BlockColor.T_YELLOW);
                case T_GREEN:
                    return spritesColors.get(BlockColor.T_GREEN);
                case T_PINK:
                    return spritesColors.get(BlockColor.T_PINK);
                case T_PURPLE:
                    return spritesColors.get(BlockColor.T_PURPLE);
                case T_RED:
                    return spritesColors.get(BlockColor.T_RED);
                case EMPTY:
                    return spritesColors.get(BlockColor.EMPTY);
                default:
                    return spritesColors.get(BlockColor.EMPTY);
            }
    }

    public void switchToTransparent() {
        switch (blockColor) {
            case BLUE:
                blockColor = BlockColor.T_BLUE;
                break;
            case ORANGE:
                blockColor = BlockColor.T_ORANGE;
                break;
            case YELLOW:
                blockColor = BlockColor.T_YELLOW;
                break;
            case GREEN:
                blockColor = BlockColor.T_GREEN;
                break;
            case PINK:
                blockColor = BlockColor.T_PINK;
                break;
            case PURPLE:
                blockColor = BlockColor.T_PURPLE;
                break;
            case RED:
                blockColor = BlockColor.T_RED;
                break;
            default:
                break;
        }
    }
}


