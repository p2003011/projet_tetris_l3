package Vue;

import Model.utils.BlockColor;
import Model.utils.Sound;

import javax.sound.sampled.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Crée par Jérémie
 * Classe qui gère les sons du jeu
 * Le design pattern Singleton est utilisé pour cette classe car tout les son doivent être géré par une seule instance
 */
public class AudioManager {
    private static AudioManager instance = null;

    private HashMap<Sound, String> audioFiles;
    private ArrayList<Clip> loopClips;

    private AudioManager() {
        loadSound();
    }

    public static AudioManager getInstance() {
        if(instance == null) {
            instance = new AudioManager();
        }
        return instance;
    }

    private void loadSound() {
        loopClips = new ArrayList<Clip>();
        try {
            audioFiles = new HashMap<Sound, String>();
            audioFiles.put(Sound.MUSIC, "src/Data/music.wav");
            audioFiles.put(Sound.MOVE, "src/Data/move.wav");
            audioFiles.put(Sound.ROTATE, "src/Data/rotate.wav");
            audioFiles.put(Sound.LINEBREAK, "src/Data/linebreak.wav");
            audioFiles.put(Sound.GAMEOVER, "src/Data/gameover.wav");
            audioFiles.put(Sound.DROP, "src/Data/drop.wav");
            audioFiles.put(Sound.SWAP, "src/Data/swap.wav");

        }
        catch (Exception e) {
            System.out.println(e);
        }
    }

    public void playSound(Sound sound, boolean loop) {
        try {
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File(audioFiles.get(sound)));
            Clip clip = AudioSystem.getClip();
            clip.open(audioInputStream);

            if (loop) {
                clip.loop(Clip.LOOP_CONTINUOUSLY);
                loopClips.add(clip);
            }
            clip.addLineListener(event -> {
                if (event.getType() == LineEvent.Type.STOP) {
                    clip.close();
                }
            });
            clip.start();

        }catch (Exception e) {
            System.out.println(e);
        }
    }

    public void stopLoopClip() {
        for (Clip clip : loopClips) {
            clip.stop();
        }
        loopClips.clear();
    }
}
