package Vue;

import Model.data.Score;
import Model.data.TableauDesScores;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Observer;

/**
 * Crée par Jérémie
 * Classe qui gère l'affichage du tableau des scores
 */
public class VueTableauDesScore extends JPanel {

    TableauDesScores listDesScores;
    public VueTableauDesScore(TableauDesScores listDesScores){
        this.listDesScores = listDesScores;
        this.setLayout(new GridLayout(listDesScores.getTableauDesScores().size(), 2));
        JLabel playerLabel;
        JLabel scoreLabel;
        for(Score score : listDesScores.getTableauDesScores()){
            playerLabel = new JLabel(score.getPlayerName(), SwingConstants.CENTER);
            scoreLabel = new JLabel(score.getScore() + "", SwingConstants.CENTER);
            this.add(playerLabel);
            this.add(scoreLabel);
        }

    }


    public void update() {
        this.removeAll();
        JLabel playerLabel;
        JLabel scoreLabel;
        this.setLayout(new GridLayout(listDesScores.getTableauDesScores().size(), 2));
        for(Score score : this.listDesScores.getTableauDesScores()){
            playerLabel = new JLabel(score.getPlayerName(), SwingConstants.CENTER);
            scoreLabel = new JLabel(score.getScore() + "", SwingConstants.CENTER);
            this.add(playerLabel);
            this.add(scoreLabel);
        }
    }
}
