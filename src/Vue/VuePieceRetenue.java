package Vue;

import Model.ATetre;
import Model.BlockTetre;

import Model.Listeners.IPieceRetenueUpdateListener;
import Model.Partie;


import Model.utils.BlockColor;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferStrategy;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;

/**
 * Crée par Jérémie
 * Classe qui gère l'affichage des piece qui seront retenu
 * Le design pattern Observer est utilisé pour cette classe car elle doit être notifié à chaque fois qu'une piece est retenu
 */
public class VuePieceRetenue extends JPanel implements Observer, IPieceRetenueUpdateListener {
    private int TAILLE = 16;
    ATetre tetreRetenu;
    Canvas c;
    Partie partie;

    private HashMap<BlockColor, Image> spriteColors;
    public VuePieceRetenue( Partie _partie, HashMap<BlockColor, Image> _spriteColors) {
        spriteColors = _spriteColors;
        partie = _partie;

        setLayout(new BorderLayout());
        Dimension dim = new Dimension(TAILLE*4,TAILLE*2);
        this.setPreferredSize(dim);
        this.setFocusable(false);


        //setBackground(Color.black);

        c = new Canvas() {
            public void paint(Graphics g) {
                Dimension actualSize = this.getSize();
                //Get blockSize from the actual size of the panel
                TAILLE = Math.min(actualSize.width / 4, actualSize.height / 2);
                g.setColor(Color.WHITE);
                g.fillRect(0, 0, TAILLE*4 , TAILLE*3);

                if(tetreRetenu == null) return;

                int maxLongueur = tetreRetenu.getLargeurTetre();
                int espace = (actualSize.width - maxLongueur * TAILLE) / 2;

                for (BlockTetre block: tetreRetenu.getListDeBlocModel())
                {
                    Image sprite = block.getSprit(spriteColors);
                    g.drawImage(sprite, block.getCoord().x * TAILLE +espace   , block.getCoord().y * TAILLE + espace/2, TAILLE  , TAILLE , null);
                    g.setColor(Color.BLACK);
                    g.drawRoundRect(block.getCoord().x * TAILLE+espace , block.getCoord().y * TAILLE + espace/2, TAILLE , TAILLE , 1, 1);
                }
            }
        };

        c.setPreferredSize(dim);
        add(c, BorderLayout.CENTER);
        this.partie.addHoldedTetreUpdateListener(this);
    }


    @Override
    public void update(Observable o, Object arg) {
        BufferStrategy bs = c.getBufferStrategy(); // bs + dispose + show : double buffering pour éviter les scintillements
        if(bs == null) {
            c.createBufferStrategy(2);
            return;
        }
        Graphics g = bs.getDrawGraphics();
        c.paint(g); // appel de la fonction pour dessiner
        g.dispose();
        //Toolkit.getDefaultToolkit().sync(); // forcer la synchronisation
        bs.show();
    }

    @Override
    public void pieceRetenueUpdate() {
        tetreRetenu = this.partie.getHoldedTetre();
        this.update(null, null);
        BufferStrategy bs = c.getBufferStrategy(); // bs + dispose + show : double buffering pour éviter les scintillements
        if(bs == null) {
            c.createBufferStrategy(2);
            return;
        }
        Graphics g = bs.getDrawGraphics();
        c.paint(g); // appel de la fonction pour dessiner
        g.dispose();
        //Toolkit.getDefaultToolkit().sync(); // forcer la synchronisation
        bs.show();

    }
}
