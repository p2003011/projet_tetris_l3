package Vue;

import Controller.FinDePartieBtnListener;
import Model.Listeners.IPartieFiniListener;
import Model.Partie;
import Model.data.Score;
import Model.data.TableauDesScores;

import javax.swing.*;
import java.awt.*;

/**
 * Crée par Jérémie
 * Classe qui gère l'affichage de la partie fini
 * Le design pattern Observer est utilisé pour cette classe car elle doit être notifié lorsque la partie est fini
 */
public class VuePartieFini extends JPanel implements IPartieFiniListener {
    private JTextField nomJoueur;
    JLabel scoreText;
    private Partie partie;
    public VuePartieFini(Partie _partie, FinDePartieBtnListener finDePartieBtnListener ){
        partie = _partie;
        nomJoueur = new JTextField();
        nomJoueur.setToolTipText("Entrez votre nom");

        nomJoueur.setMaximumSize(new Dimension(100,30));


        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.setAlignmentY(Component.CENTER_ALIGNMENT);
        JPanel panel1 = new JPanel();
        panel1.setLayout(new BoxLayout(panel1, BoxLayout.X_AXIS));
        panel1.add(new JLabel("Partie fini"));
        this.add(panel1);

        JPanel panel2 = new JPanel();
        panel2.setLayout(new BoxLayout(panel2, BoxLayout.X_AXIS));
        scoreText = new JLabel("Votre score : " + partie.getScore().getScore());
        panel2.add(scoreText);
        this.add(panel2);

        JPanel panel3 = new JPanel();
        panel3.setLayout(new BoxLayout(panel3, BoxLayout.X_AXIS));
        panel3.add(nomJoueur);
        this.add(panel3);

        JPanel panel4 = new JPanel();
        panel4.setLayout(new BoxLayout(panel4, BoxLayout.X_AXIS));
        JButton enregistrer = new JButton("Enregistrer");
        enregistrer.addActionListener(finDePartieBtnListener);
        panel4.add(enregistrer);
        this.add(panel4);

        partie.addPartieFiniListener(this);
    }

    public Score getScore(){
        return partie.getScore();
    }

    public String getNom(){
        return nomJoueur.getText();
    }

    @Override
    public void partieFini() {
        scoreText.setText("Votre score : " + partie.getScore().getScore());
    }
}
