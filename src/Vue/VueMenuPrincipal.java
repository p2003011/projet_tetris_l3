package Vue;

import Controller.VuePrincipaleListener;
import Model.data.TableauDesScores;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Observable;
import java.util.Observer;

import static java.lang.System.exit;

/**
 * Crée par Jérémie
 * Classe qui gère l'affichage du menu principal avec l'affichage du tableau des scores
 */
public class VueMenuPrincipal extends JPanel  {

    VueTableauDesScore vueTableauDesScore;
    JButton jouer;
    JButton quitter;
    JButton difficulte;
    JPanel menu;



    public VueMenuPrincipal(TableauDesScores tableauDesScores, VuePrincipaleListener vuePrincipaleListener){
        vueTableauDesScore = new VueTableauDesScore(tableauDesScores);
        JLabel titre = new JLabel("Tetris", SwingConstants.CENTER);

        this.setLayout(new BorderLayout());
        menu = new JPanel();
        menu.setLayout(new BoxLayout(menu, BoxLayout.X_AXIS));

        jouer = new JButton("Jouer");
        jouer.addActionListener(vuePrincipaleListener.getSwichToJeu());
        quitter = new JButton("Quitter");
        quitter.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                exit(0);
            }
        });
        difficulte = new JButton("Difficulté");
        menu.add(Box.createHorizontalGlue());
        menu.add(jouer);;
        menu.add(Box.createHorizontalGlue());
        menu.add(quitter);
        menu.add(Box.createHorizontalGlue());
        menu.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        menu.setBackground(Color.RED);

        this.add(titre, BorderLayout.NORTH);
        this.add(vueTableauDesScore, BorderLayout.CENTER);
        this.add(menu, BorderLayout.SOUTH);


    }

}
