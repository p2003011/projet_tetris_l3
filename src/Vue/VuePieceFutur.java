package Vue;

import Model.ATetre;
import Model.BlockTetre;

import Model.Listeners.IPieceFuturUpdateListener;
import Model.Partie;

import Model.utils.BlockColor;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferStrategy;

import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;

/**
 * Crée par Jérémie
 * Classe qui gère l'affichage des piece qui seront choisies par la suite
 * Le design pattern Observer est utilisé pour cette classe car elle doit être notifié à chaque fois qu'une piece est posé
 */
public class VuePieceFutur extends JPanel implements IPieceFuturUpdateListener {
    private static int TAILLE = 16;
    private Partie partie;
    Canvas c;
    private HashMap<BlockColor, Image> spriteColors;
    public VuePieceFutur(Partie _partie, HashMap<BlockColor, Image> _spriteColors) {
        spriteColors = _spriteColors;
        partie = _partie;
        setLayout(new BorderLayout());
        Dimension dim = new Dimension(TAILLE*4,TAILLE*2* partie.getNextTetreList().size());
        this.setPreferredSize(dim);
        this.setFocusable(false);


        //setBackground(Color.black);

        c = new Canvas() {
            public void paint(Graphics g) {
                Dimension actualSize = this.getSize();
                //Get blockSize from the actual size of the panel
                TAILLE = Math.min(actualSize.width / 4, actualSize.height / (2* partie.getNextTetreList().size()));
                g.setColor(Color.WHITE);
                g.fillRect(0, 0, TAILLE*4 , TAILLE*3 * partie.getNextTetreList().size());

                int nbTetre = 0;
                try {
                    for(ATetre tetre : partie.getNextTetreList()){

                        int maxLongueur = tetre.getLargeurTetre();
                        int espace = (actualSize.width - maxLongueur * TAILLE) / 2;

                        for (BlockTetre block: tetre.getListDeBlocModel())
                        {
                            Image sprite = block.getSprit(spriteColors);

                            g.drawImage(sprite, block.getCoord().x * TAILLE +espace, (block.getCoord().y + 3*nbTetre) * TAILLE + espace/2 , TAILLE  , TAILLE , null);
                            g.setColor(Color.BLACK);
                            g.drawRoundRect(block.getCoord().x * TAILLE+espace, (block.getCoord().y + 3*nbTetre) * TAILLE + espace/2, TAILLE , TAILLE , 1, 1);
                        }
                        nbTetre++;
                    }
                }catch (Exception e){

                }
            }
        };

        c.setPreferredSize(dim);
        add(c, BorderLayout.CENTER);
        this.partie.addNextTetreUpdateListener(this);
    }


    @Override
    public void pieceFuturUpdate() {
        BufferStrategy bs = c.getBufferStrategy(); // bs + dispose + show : double buffering pour éviter les scintillements
        if(bs == null) {
            c.createBufferStrategy(2);
            return;
        }
        Graphics g = bs.getDrawGraphics();
        c.paint(g);
        g.dispose();
        //Toolkit.getDefaultToolkit().sync(); // forcer la synchronisation
        bs.show();
    }
}
