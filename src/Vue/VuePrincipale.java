package Vue;

import Controller.FinDePartieBtnListener;
import Controller.VueJeuListener;
import Controller.VuePrincipaleListener;
import Model.Listeners.IHitWallListener;
import Model.Listeners.IlineCompletListener;
import Model.Partie;
import Model.data.TableauDesScores;
import Model.utils.Movement;


import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowStateListener;

/**
 * Crée par Jérémie, modifié par Théo
 * Classe qui gère l'affichage des deux vu principale, celle du menu (score) et celle du jeu
 * Cette classe est un observer
 * elle est notifié par la partie pour les changement d'état
 */
public class VuePrincipale extends JFrame implements IlineCompletListener, IHitWallListener, WindowStateListener {

    VueJeu vueJeu;
    VueMenuPrincipal vueMenuPrincipal;

    VuePrincipaleListener vuePrincipaleListener;

    VueJeuListener vueJeuListener;

    JButton phamtomBackToMenuButton;


    public VuePrincipale(){
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setLayout(new CardLayout());

        this.setPreferredSize(new Dimension(600, 600));
    }

    public void init(VuePrincipaleListener vuePrincipaleListener, VueJeuListener _vueJeuListener, FinDePartieBtnListener partieFiniListener, Partie partie, TableauDesScores tableauDesScores){
        vueJeuListener = _vueJeuListener;
        vueJeu = new VueJeu(partie, vueJeuListener, partieFiniListener);
        vueMenuPrincipal = new VueMenuPrincipal(tableauDesScores, vuePrincipaleListener);
        phamtomBackToMenuButton = new JButton();
        phamtomBackToMenuButton.addActionListener(vuePrincipaleListener.getSwichToMenuPrincipal());

        //phamtomBackToMenuButton.setVisible(false);
        this.add(phamtomBackToMenuButton);
        KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        manager.addKeyEventDispatcher(vuePrincipaleListener.getKeySwichToMenuPrincipal());
        partie.addLineCompletListener(this);
        partie.addHitWallListener(this);


        this.add(vueJeu, "jeu");
        this.add(vueMenuPrincipal, "menu");
        this.show("menu");
        this.pack();
        this.setVisible(true);
        this.addWindowStateListener(this);

 
        this.setFocusable(true);

    }

    public void show(String name){
        CardLayout cl = (CardLayout)(this.getContentPane().getLayout());
        cl.show(this.getContentPane(), name);
    }


    public VuePieceRetenue getVuePieceRetenue() {
    	return vueJeu.vuePieceRetenue;
    }

    public VueJeu getVueJeu() {
        return vueJeu;
    }

    public void shake(int power) {
        final int duration = 200; // Durée de la secousse en millisecondes

        Point originalLocation = this.getLocation();

        long startTime = System.currentTimeMillis();

        Thread shakeThread = new Thread(() -> {
            while (System.currentTimeMillis() - startTime < duration) {
                int deltaX = (int) (Math.sin(System.currentTimeMillis() * 0.1) * power);
                int deltaY = (int) (Math.cos(System.currentTimeMillis() * 0.1) * power);

                this.setLocation(originalLocation.x + deltaX, originalLocation.y + deltaY);

                try {
                    Thread.sleep(16); // Attente pour ajuster la fréquence
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            SwingUtilities.invokeLater(() -> this.setLocation(originalLocation));
        });

        shakeThread.start();
    }



    @Override
    public void LineComplet(int nbLigne) {
        shake(nbLigne*2);
    }

    @Override
    public void hitWall(Movement movement) {

    }

    public VueTableauDesScore getVueScore() {
    	return this.vueMenuPrincipal.vueTableauDesScore;
    }

    @Override
    public void windowStateChanged(WindowEvent e) {
        this.vueJeu.componentResized(null);
    }




}
