package Vue;

import Controller.FinDePartieBtnListener;
import Controller.VueJeuListener;
import Controller.reprendreJeuListener;
import Model.Listeners.IPartieFiniListener;
import Model.Listeners.ISetJeuListener;
import Model.Listeners.ISetPauseListener;
import Model.Partie;
import Model.utils.BlockColor;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.*;

/**
 * Crée par Jérémie
 * Classe qui gère l'affichage jeu en lui même
 * Le design pattern Observer est utilisé pour cette classe car elle doit être notifié pour chaque changement d'état de la partie (pause, jouer, fin de partie)
 */
public class VueJeu extends JPanel implements ComponentListener, IPartieFiniListener, ISetJeuListener, ISetPauseListener {

    VueGrille vueGrille;
    VueScorePartie vueScorePartie;

    public VuePieceRetenue vuePieceRetenue;

    VuePieceFutur vuePieceFutur;

    VuePartieFini vuePartieFini;

    HashMap<BlockColor, Image> spriteColors;

    Partie partie;

    JPanel leftPanel;

    JPanel centerPanel;

    JPanel vuepause;

    FinDePartieBtnListener partieFiniListener;

    public VueJeu(Partie partie, VueJeuListener vueJeuListener, FinDePartieBtnListener partieFiniListener){
        loadTexture();
        this.partieFiniListener = partieFiniListener;
        this.partie = partie;
        this.setLayout(new BorderLayout());
        vueGrille = new VueGrille(partie, vueJeuListener, spriteColors);
        vueScorePartie = new VueScorePartie(partie);


        this.partie.addPartieFiniListener(this);
        this.partie.addSetJeuListener(this);
        this.partie.addSetPauseListener(this);


        vuePieceFutur = new VuePieceFutur(partie, spriteColors);

        leftPanel = new JPanel();
        leftPanel.setLayout(new GridLayout(2, 1));
        vuePieceRetenue = new VuePieceRetenue(partie, spriteColors);
        leftPanel.add(vuePieceRetenue);
        leftPanel.add(vueScorePartie);

        centerPanel = new JPanel();
        //set cardlayout

        centerPanel.setLayout(new CardLayout());
        vuePartieFini = new VuePartieFini(partie,partieFiniListener);
        centerPanel.add(vueGrille, "jeu");

        centerPanel.add(vuePartieFini, "fini");
        vuepause = new JPanel();
        vuepause.setLayout(new GridLayout(2,1));
        vuepause.add(new JLabel("Pause"));
        JButton btn = new JButton("Reprendre");
        btn.addActionListener(new reprendreJeuListener(partie));
        vuepause.add(btn);
        centerPanel.add(vuepause, "pause");
        //show jeu
        CardLayout cl = (CardLayout)(centerPanel.getLayout());
        cl.show(centerPanel, "jeu");
        this.add(centerPanel, BorderLayout.CENTER);
        this.add(leftPanel, BorderLayout.WEST);
        this.add(vuePieceFutur, BorderLayout.EAST);
        this.addComponentListener(this);
        this.setFocusable(false);



    }

    private void loadTexture(){
        //load blockImage from Data folder
        spriteColors = new HashMap<BlockColor, Image>();
        spriteColors.put(BlockColor.BLUE, new ImageIcon("src/Data/blocktetre_blue.png").getImage());
        spriteColors.put(BlockColor.GREEN, new ImageIcon("src/Data/blocktetre_green.png").getImage());
        spriteColors.put(BlockColor.ORANGE, new ImageIcon("src/Data/blocktetre_orange.png").getImage());
        spriteColors.put(BlockColor.PINK, new ImageIcon("src/Data/blocktetre_pink.png").getImage());
        spriteColors.put(BlockColor.RED, new ImageIcon("src/Data/blocktetre_red.png").getImage());
        spriteColors.put(BlockColor.PURPLE, new ImageIcon("src/Data/blocktetre_purple.png").getImage());
        spriteColors.put(BlockColor.YELLOW, new ImageIcon("src/Data/blocktetre_yellow.png").getImage());
        spriteColors.put(BlockColor.T_BLUE, new ImageIcon("src/Data/blocktetre_transparent_blue.png").getImage());
        spriteColors.put(BlockColor.T_GREEN, new ImageIcon("src/Data/blocktetre_transparent_green.png").getImage());
        spriteColors.put(BlockColor.T_ORANGE, new ImageIcon("src/Data/blocktetre_transparent_orange.png").getImage());
        spriteColors.put(BlockColor.T_PINK, new ImageIcon("src/Data/blocktetre_transparent_pink.png").getImage());
        spriteColors.put(BlockColor.T_PURPLE, new ImageIcon("src/Data/blocktetre_transparent_purple.png").getImage());
        spriteColors.put(BlockColor.T_RED, new ImageIcon("src/Data/blocktetre_transparent_red.png").getImage());
        spriteColors.put(BlockColor.T_YELLOW, new ImageIcon("src/Data/blocktetre_transparent_yellow.png").getImage());
    }



    @Override
    public void componentResized(ComponentEvent e) {
        Dimension d = vueGrille.getSize();
        vueGrille.setPreferredSize(new Dimension((int)(d.width * 0.6), d.height));
        leftPanel.setPreferredSize(new Dimension((int)(d.width * 0.2), d.height));
        vuePieceFutur.setPreferredSize(new Dimension((int)(d.width * 0.2), d.height));
        this.revalidate();
        this.repaint();
    }

    @Override
    public void componentMoved(ComponentEvent e) {

    }

    @Override
    public void componentShown(ComponentEvent e) {

    }

    @Override
    public void componentHidden(ComponentEvent e) {

    }


    @Override
    public void partieFini() {

        CardLayout cl = (CardLayout)(centerPanel.getLayout());
        cl.show(centerPanel, "fini");
    }

    @Override
    public void partiePause() {

        CardLayout cl = (CardLayout)(centerPanel.getLayout());
        cl.show(centerPanel, "pause");
    }

    @Override
    public void partieJeu() {

        CardLayout cl = (CardLayout)(centerPanel.getLayout());
        cl.show(centerPanel, "jeu");
        this.vueGrille.c.requestFocus();
    }

    public VuePartieFini getVuePartieFini() {
        return vuePartieFini;
    }

    public VueGrille getVueGrille() {
        return vueGrille;
    }
}
