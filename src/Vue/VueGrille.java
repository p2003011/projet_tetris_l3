package Vue;

import Controller.VueJeuListener;
import Model.ATetre;
import Model.BlockTetre;
import Model.Listeners.IGrilleUpdateListener;
import Model.Partie;
import Model.utils.BlockColor;

import javax.swing.*;
import java.awt.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.image.BufferStrategy;
import java.util.HashMap;


/**
 * Crée par Jérémie
 * Classe qui gère l'affichage de la grille de jeu
 * Le design pattern Observer est utilisé pour cette classe car elle doit être notifié à chaque fois que la grille est mise à jour
 */
public class VueGrille extends JPanel implements IGrilleUpdateListener, FocusListener {

    private static int TAILLE = 16;
    private ATetre pieceCourante;
    private HashMap<BlockColor, Image> spriteColors;
    private Partie partie;

    private VueJeuListener vueJeuListener;
    public Canvas c;




    public VueGrille(Partie _partie, VueJeuListener vueJeuListener, HashMap<BlockColor, Image> _spriteColors) {
        spriteColors = _spriteColors;
        partie = _partie;
        this.vueJeuListener = vueJeuListener;

        setLayout(new BorderLayout());
        Dimension dim = new Dimension(TAILLE* partie.getGrille().getLargeur(),TAILLE*partie.getGrille().getHauteur());
        this.setPreferredSize(dim);


        //setBackground(Color.black);

        c = new Canvas() {
            public void paint(Graphics g) {
            Dimension actualSize = this.getSize();
            //Get blockSize from the actual size of the panel
            TAILLE = Math.min(actualSize.width / partie.getGrille().getLargeur(), actualSize.height / partie.getGrille().getHauteur());
            int borderSpace = (actualSize.width - partie.getGrille().getLargeur() * TAILLE) / 2;

                for (int i = 0; i < partie.getGrille().getHauteur(); i++) {
                    for (int j = 0; j < partie.getGrille().getLargeur(); j++) {
                        //if (!(i == exempleDeBase.modele.getPieceCourante().getx() && j == exempleDeBase.modele.getPieceCourante().gety())) {
                        if (partie.getGrille().getCase(j,i) == null) {

                            if (i>2){
                                g.setColor(Color.WHITE);
                                g.fillRect(j* TAILLE + 2 + borderSpace, i * TAILLE + 2, TAILLE -2, TAILLE -2);
                                g.setColor(Color.BLACK);
                                g.drawRect(j * TAILLE +borderSpace, i * TAILLE, TAILLE, TAILLE);
                                g.drawRect(j * TAILLE + 1+borderSpace, i * TAILLE + 1, TAILLE-1, TAILLE-1);
                            }
                            else {
                                g.setColor(Color.WHITE);
                                g.fillRect(j* TAILLE  + borderSpace, i * TAILLE , TAILLE , TAILLE );
                            }



                        }
                        else {
                            g.setColor(Color.WHITE);
                            Image sprite = partie.getGrille().getCase(j,i).getSprit(spriteColors);

                            g.drawImage(sprite, j* TAILLE + 2 +borderSpace, i * TAILLE + 2, TAILLE -2, TAILLE -2, null);

                            g.setColor(Color.BLACK);
                            g.drawRect(j * TAILLE+borderSpace, i * TAILLE, TAILLE, TAILLE);
                            g.drawRect(j * TAILLE + 1+borderSpace, i * TAILLE + 1, TAILLE-1, TAILLE-1);
                        }
                    }

                }
                if(pieceCourante == null)
                    return;
                for (BlockTetre block: pieceCourante.getListeDeBlock())
                {
                    Image sprite = block.getSprit(spriteColors);
                    g.drawImage(sprite, block.getCoord().x* TAILLE + 2 +borderSpace, block.getCoord().y * TAILLE + 2, TAILLE -2, TAILLE -2, null);
                    g.setColor(Color.BLACK);
                    g.drawRect(block.getCoord().x * TAILLE+borderSpace, block.getCoord().y * TAILLE, TAILLE, TAILLE);
                    g.drawRect(block.getCoord().x * TAILLE + 1+borderSpace, block.getCoord().y * TAILLE + 1, TAILLE-1, TAILLE-1);
                }

                BlockTetre[] pieceCouranteLowestBlockPos = pieceCourante.getLowestBlockPos();
                if (pieceCouranteLowestBlockPos != null) {
                    for (BlockTetre block : pieceCouranteLowestBlockPos) {
                        Image sprite = block.getSprit(spriteColors);
                        g.drawImage(sprite, block.getCoord().x* TAILLE + 2 +borderSpace, block.getCoord().y * TAILLE + 2, TAILLE -2, TAILLE -2, null);
                        g.setColor(Color.BLACK);
                        g.drawRect(block.getCoord().x * TAILLE+borderSpace, block.getCoord().y * TAILLE, TAILLE, TAILLE);
                        g.drawRect(block.getCoord().x * TAILLE + 1+borderSpace, block.getCoord().y * TAILLE + 1, TAILLE-1, TAILLE-1);
                    }
                }
            }
        };

        c.setPreferredSize(dim);
        c.addKeyListener(vueJeuListener);
        add(c, BorderLayout.CENTER);
        this.setFocusable(false);
        this.partie.addGrilleUpdateListener(this);

    }

    public void grilleUpdate() {

        this.pieceCourante = partie.getCurrentTetre();
        BufferStrategy bs = c.getBufferStrategy(); // bs + dispose + show : double buffering pour éviter les scintillements
        if(bs == null) {
            c.createBufferStrategy(2);
            return;
        }
        Graphics g = bs.getDrawGraphics();
        c.paint(g); // appel de la fonction pour dessiner
        g.dispose();
        //Toolkit.getDefaultToolkit().sync(); // forcer la synchronisation
        bs.show();
    }

    @Override
    public void focusGained(FocusEvent e) {
        addKeyListener(vueJeuListener);
    }

    @Override
    public void focusLost(FocusEvent e) {
        removeKeyListener(vueJeuListener);
    }
}
