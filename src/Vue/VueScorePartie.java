package Vue;

import Model.Listeners.IScoreUpdateListener;
import Model.Partie;

import javax.swing.*;
import java.awt.*;

/**
 * Crée par Jérémie
 * Classe qui gère l'affichage du score de la partie
 * Le design pattern Observer est utilisé pour cette classe car elle doit être notifié lorsque le score, le nb de ligne ou le niveau change
 */
public class VueScorePartie extends JPanel implements IScoreUpdateListener {
    Partie partie;
    JLabel scoreLabel;
    JLabel niveauLabel;
    JLabel ligneLabel;



    VueScorePartie(Partie _partie){
        partie = _partie;
        this.setLayout(new GridLayout(4, 1));
        scoreLabel = new JLabel("Score : " + partie.getScore().getScore());
        niveauLabel = new JLabel("Niveau : " + partie.getScore().getNiveau());
        ligneLabel = new JLabel("Ligne : " + partie.getScore().getNbLigne());

        this.add(scoreLabel);
        this.add(niveauLabel);
        this.add(ligneLabel);
        this.setFocusable(false);
        partie.addScoreUpdateListener(this);
    }

    @Override
    public void scoreUpdate() {
        scoreLabel.setText("Score : " + partie.getScore().getScore());
        niveauLabel.setText("Niveau : " + partie.getScore().getNiveau());
        ligneLabel.setText("Ligne : " + partie.getScore().getNbLigne());
    }


}
