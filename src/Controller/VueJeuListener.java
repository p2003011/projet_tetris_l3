package Controller;

import Model.Partie;
import Model.utils.Movement;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Timer;
import java.util.TimerTask;


/**
 * Crée par Jérémie
 * Gère les inputs en jeu
 * Elle implémente KeyListener pour pouvoir observer les inputs
 * Les inputs des déplacements sont gérés par des timers pour pouvoir faire des déplacements continus
 */
public class VueJeuListener implements KeyListener {
    private Boolean rotateRelease = true;
    private Boolean dropRelease = true;

    private Boolean leftRelease = true;
    private Boolean rightRelease = true;
    private Boolean downRelease = true;

    private Boolean holdTetre = true;

    private Timer timerLeft;
    private Timer timerRight;
    private Timer timerDown;

    private TimerTask leftTask;
    private TimerTask rightTask;
    private TimerTask downTask;
    
    private static final int DELAY = 120;


    private Partie partie;


    public VueJeuListener(Partie partie) {
        this.partie = partie;
        timerLeft = new Timer();
        timerRight = new Timer();
        timerDown = new Timer();
        leftTask = new TimerTask() {
            @Override
            public void run() {
                partie.move(Movement.GAUCHE);
            }
        };
        rightTask = new TimerTask() {
            @Override
            public void run() {
                partie.move(Movement.DROITE);
            }
        };
        downTask = new TimerTask() {
            @Override
            public void run() {
                partie.move(Movement.BAS);
            }
        };

    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_DOWN && downRelease) {
            timerDown.scheduleAtFixedRate(downTask, 0, DELAY);
            downRelease = false;
        }
        if (e.getKeyCode() == KeyEvent.VK_LEFT && leftRelease) {
            timerLeft.scheduleAtFixedRate(leftTask, 0, DELAY);
            leftRelease = false;
        }
        if (e.getKeyCode() == KeyEvent.VK_RIGHT && rightRelease) {
            timerRight.scheduleAtFixedRate(rightTask, 0, DELAY);
            rightRelease = false;
        }


        if (e.getKeyCode() == KeyEvent.VK_SPACE && dropRelease) {
            partie.move(Movement.DROP);
            dropRelease = false;
        }
        if (e.getKeyCode() == KeyEvent.VK_UP && rotateRelease) {
            partie.move(Movement.ROTATE);
            rotateRelease = false;
        }
        if (e.getKeyCode() == KeyEvent.VK_C && holdTetre) {
            partie.exchangeHoldedTetre();
            holdTetre = false;
        }

        if (e.getKeyCode() == KeyEvent.VK_P && !partie.getIsPaused()) {
            partie.togglePause();
        }
    }


    @Override
    public void keyReleased(KeyEvent e) {
        switch (e.getKeyCode()){
            case KeyEvent.VK_SPACE:
                dropRelease = true;
            case KeyEvent.VK_UP:
                rotateRelease = true;
            case KeyEvent.VK_DOWN:
                downTask.cancel();
                downTask = new TimerTask() {
                    @Override
                    public void run() {
                        partie.move(Movement.BAS);
                    }
                };
                downRelease = true;
            case KeyEvent.VK_LEFT:
                leftTask.cancel();
                leftTask = new TimerTask() {
                    @Override
                    public void run() {
                        partie.move(Movement.GAUCHE);
                    }
                };
                leftRelease = true;
            case KeyEvent.VK_RIGHT:
                rightTask.cancel();
                rightTask = new TimerTask() {
                    @Override
                    public void run() {
                        partie.move(Movement.DROITE);
                    }
                };
                rightRelease = true;
            case KeyEvent.VK_C:
                holdTetre = true;
        }
    }

}
