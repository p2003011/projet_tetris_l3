package Controller;

import Model.data.Score;
import Model.data.TableauDesScores;
import Vue.VuePartieFini;
import Vue.VuePrincipale;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


/**
 * Crée par Jérémie le 10/12/2019
 * Gère le bouton de fin de partie
 * Elle implémente ActionListener pour pouvoir observer le bouton
 * Lorque le bouton est cliqué, elle ajoute le score dans le tableau des scores et retourne au menu principal
 */
public class FinDePartieBtnListener implements ActionListener {
    VuePrincipale vuePrincipale;

    TableauDesScores tableauDesScores;

    public FinDePartieBtnListener(VuePrincipale vuePrincipale, TableauDesScores tableauDesScores){
        this.vuePrincipale = vuePrincipale;
        this.tableauDesScores = tableauDesScores;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Score score = new Score(vuePrincipale.getVueJeu().getVuePartieFini().getScore().getScore(), vuePrincipale.getVueJeu().getVuePartieFini().getNom());
        tableauDesScores.addScore(score);
        vuePrincipale.show("menu");
        vuePrincipale.getVueScore().update();
    }
}
