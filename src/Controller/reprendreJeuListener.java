package Controller;

import Model.Partie;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Crée par Jérémie
 * Gère le bouton de reprendre le jeu
 * Elle implémente ActionListener pour pouvoir observer le bouton
 */
public class reprendreJeuListener implements ActionListener {
    Partie partie;


    public reprendreJeuListener(Partie partie) {
        this.partie = partie;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        partie.togglePause();
    }
}
