package Controller;

import Model.Partie;
import Vue.VuePrincipale;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

/**
 * Crée par Jérémie
 * Gère les passage entre le menu principal et le jeu
 */
public class VuePrincipaleListener {
    SwichToMenuPrincipal swichToMenuPrincipal;

    keySwichToMenuPrincipal keySwichToMenuPrincipal;
    SwichToJeu swichToJeu;
    Partie partie;

    public VuePrincipaleListener(VuePrincipale vuePrincipale, Partie partie){
        swichToMenuPrincipal = new SwichToMenuPrincipal(vuePrincipale);
        swichToJeu = new SwichToJeu(vuePrincipale);
        keySwichToMenuPrincipal = new keySwichToMenuPrincipal(vuePrincipale);

        this.partie = partie;
    }

    class SwichToMenuPrincipal implements ActionListener {
        VuePrincipale vuePrincipale;

        public SwichToMenuPrincipal(VuePrincipale vuePrincipale){
            this.vuePrincipale = vuePrincipale;
        }
        @Override
        public void actionPerformed(ActionEvent e) {
            vuePrincipale.show("menu");
        }
    }

    class keySwichToMenuPrincipal implements KeyEventDispatcher {
        VuePrincipale vuePrincipale;

        public keySwichToMenuPrincipal(VuePrincipale vuePrincipale){
            this.vuePrincipale = vuePrincipale;
        }

        @Override
        public boolean dispatchKeyEvent(KeyEvent e) {


            if (e.getID() == KeyEvent.KEY_PRESSED) {
                if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                    vuePrincipale.show("menu");
                }
            }
            return false;
        }
    }

    class SwichToJeu implements ActionListener {
        VuePrincipale vuePrincipale;

        public SwichToJeu(VuePrincipale vuePrincipale){
            this.vuePrincipale = vuePrincipale;
        }
        @Override
        public void actionPerformed(ActionEvent e) {

            partie.reset();
            vuePrincipale.show("jeu");
            vuePrincipale.getVueJeu().getVueGrille().c.requestFocus();
            vuePrincipale.getVueJeu().partieJeu();
        }
    }

    public SwichToMenuPrincipal getSwichToMenuPrincipal() {
        return swichToMenuPrincipal;
    }

    public SwichToJeu getSwichToJeu() {
        return swichToJeu;
    }

    public VuePrincipaleListener.keySwichToMenuPrincipal getKeySwichToMenuPrincipal() {
        return keySwichToMenuPrincipal;
    }
}
