import Controller.FinDePartieBtnListener;
import Controller.VueJeuListener;
import Controller.VuePrincipaleListener;
import Model.Partie;
import Model.data.Grille;
import Model.data.TableauDesScores;
import Vue.VuePrincipale;

public class main {
    public static void main(String arg[]){

        Grille grille = new Grille(10,23);
        Partie partie = new Partie(grille);

        VuePrincipale vuePrincipale = new VuePrincipale();

        VuePrincipaleListener vuePrincipaleListener = new VuePrincipaleListener(vuePrincipale, partie);

        TableauDesScores tableauDesScores = new TableauDesScores();

        VueJeuListener vueJeuListener = new VueJeuListener(partie);
        FinDePartieBtnListener partieFiniListener = new FinDePartieBtnListener(vuePrincipale, tableauDesScores);
        vuePrincipale.init(vuePrincipaleListener, vueJeuListener,partieFiniListener,partie, tableauDesScores);

    }
}
