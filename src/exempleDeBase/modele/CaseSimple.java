package exempleDeBase.modele;



public class CaseSimple implements Runnable {
// Fields
    private int x = 5;
    private int y = 5;
    private int dY = -1;

    private int codeCouleur = 5;
    private GrilleSimple grille;

// Getters

    public int getCodeCouleur() {
        return codeCouleur;
    }

    public int getx() {
        return x;
    }

    public int gety() {
        return y;
    }

// Constructor
    public CaseSimple(GrilleSimple _grille) {
        grille = _grille;
    }

    public void action() {
        dY *= -1;
    }

    public void run() {
        int nextY = y;
        int nextX = x;

        nextY += dY;

        if (grille.validationPosition(nextX, nextY)) {
            y = nextY;
            x = nextX;
        } else {
            dY *= -1;
        }


    }





}
