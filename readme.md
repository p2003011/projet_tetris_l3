# Projet Tetris 
> Jérémie Vernay - Théo Labrosse 

Voici le projet de LIFPOO.
Il s'agit d'un jeu de type Tetris.

## Installation
Version de JavaJDK minimum : 19

## Lancement
Pour lancer le jeu, il suffit de choisir le fichier main du dossier src dans la configuration du projet.
Puis lancer le main.

## Commandes

### Déplacement
- Flèche de gauche : Déplacement à gauche
- Flèche de droite : Déplacement à droite
- Flèche du haut : Rotation
- Flèche du bas : Accélération de la chute
- Espace : Chute instantanée

### Autres
- C : Changer de pièce
- P : Pause
- Echappe : Abandon de la partie en cours

## Fonctionnalités
- Affichage qui s'addapte à la taille de la fenêtre
- Affichage du score
- Enregistrement des scores dans un fichier
- Prévisualisation de la chute de la pièce
- Affichage des pièces suivantes
- Pièces retenues
- Systeme de difficulté qui augmente en fonction du niveau
- Musique de fond et bruitages (fait maison)
- Utilisation de sprites pour les pièces
- Effet de tremblement lors de la destruction de lignes
- System de pause
- System de Game Over
- Wall kick (déplacement de la pièce lorsqu'elle touche un mur)
- Ralentissement de la chute de la pièce lorsqu'elle touche le sol et que le joueur bouge la piece
- quelques de design pattern (Observer, Singleton, Template Method)

## Doc
Les classes sont documentées en Javadoc

Les classes faisant fonctionner le jeu sont réparties en respectant le pattern MVC.

Toute les donnée du jeu (textures, son, scores) sont stockées dans le dossier Data.

Nous avons garder les fichier de l'exemple de MVC donné par le professeur pour nous aider à comprendre le pattern dans le dossier exempleDeBase.

## Design Pattern
### Observer

Le design pattern observer est essentiel au mvc, car il permet de mettre en place un système de notification entre les objets de la vue et les objets du model.

Les Listener, implémentés par les vues (notamment les JPanels), sont passés à une classe du model qui a pour devoir de notifier les listeners à chaque changement de données qui devrait influencer sur les vues.

### Singleton

Le design pattern Singleton est utilisé quand il est nécessaire d'avoir seulement 1 instance d'une classe.
C'est le cas pour la classe AudioManager (dossier Vue) qui gère les musiques et les sons.

### Template Method

Template Method permet de mettre une série d'étapes d’un algorithme dans une classe mère.
En laissant les sous-classes redéfinir certaines étapes de l’algorithme sans changer sa structure.
Il est utilisé ici dans ATetre (Model) qui est la classe Abstraite mère des Tetromino.

Template Method a été utilisé ici pour simplifier l'algorithme de mouvement, et particulièrement de rotation.
Le tetromino Carré aura toutes les étapes de la rotation désactivées par exemple, alors que le tetromino du L aura simplement une autre fonction pour obtenir les valeurs de décalage pour faire un wall kick.

## Information supplémentaires

Le code dans le dossier ExempleDeBase n'est pas notre code, il s'agit du programme qui nous a été passé en exemple au moment de commencer le projet.
Nous ne l'avons pas utilisé, et nous sommes partis de rien pour créer ce jeu.